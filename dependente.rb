class Dependente

  attr_acessor :nome :idade :parentesco

  def initialize params
    @nome = params[:nome]
    @idade = params[:idade]
    @parentesco = params[:parentesco]
  end
end