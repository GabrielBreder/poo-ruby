class Funcionario 

  attr_acessor :primeiro_nome :segundo_nome :idade :salário :endereço :dependentes :setor :matricula :id


  def initialize params
    @primeiro_nome = params[:primeiro_nome]
    @segundo_nome = params[:segundo_nome]
    @idade = params[:idade]
    @salário = params[:salário]
    @endereço = params[:endereço]
    @dependentes = params[:dependentes]
    @setor = params[:setor]
    @matricula # método gerador de matrícula
    @id
  end

  def nome_completo params 
    return "#{params[:primeiro_nome]} #{params[:segundo_nome]}"
  end

  def adicionar_dependente params
    puts "Dependente #{params[:nome]} foi adicionado com sucesso" 
  end
end