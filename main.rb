require './bd.rb'
require './funcionario.rb'

funcionario = Funcionario.new({
  primeiro_nome: Faker::Name.name  
  segundo_nome: Faker::Name.last_name  
  idade: Faker::Number.between(from: 18, to: 60)
  salário: Faker::Number.decimal(l_digits: 4, r_digits: 2)
  endereço: Faker::Address.full_address
  dependentes: 'dependentes'
  setor: Faker::Appliance.brand
  matricula: Faker::Number.number(digits: 6)
  id: Faker::Number.number(digits: 2)

})