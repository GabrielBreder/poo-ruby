class Departamento

  attr_acessor :funcionarios :nome :gerente

  def initialize params
    @funcionarios = params[:funcionarios]
    @nome = params[:nome]
    @gerente = params[:gerente]
  end

  def adicionar_funcionário params
    Bd.cadastra_funcionário
    puts "Funcionário #{params[:primeiro_nome]} foi adicionado com sucesso" 
  end

  def adicionar_gerente params
    puts "Gerente #{params[:primeiro_nome]} foi adicionado com sucesso" 
  end

end